# x3 wiki

A proxy for [Fandom Wikis](https://www.fandom.com/) that reformats the page 
for ease of use/ad and tracking removal.

MIT license, see LICENSE.

This is a MediaWiki extension. Clone this repository into a folder named 
`X3Wiki` in your `extensions/` directory, then add `wfLoadExtension( 'X3Wiki' );`
to `LocalSettings.php`.

