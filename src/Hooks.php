<?php

namespace MediaWiki\Extension\X3Wiki;
use DOMDocument;

function is_fandom_wiki( $wiki ) {
	if ( file_get_contents( "https://".$wiki.".fandom.com" ) ) {
		if ( strtolower($wiki) != "special" and strtolower($wiki) != "project" ) {
			return true;
		}
	}

	return false;
}

class Hooks implements \MediaWiki\Hook\TitleIsAlwaysKnownHook,
					   \MediaWiki\Hook\ParserAfterTidyHook {

	public function onTitleIsAlwaysKnown( $title, &$isKnown ): bool {
		$isKnown = true;
		return true;
	}

	public function onParserAfterTidy( $parser, &$text ) {
		// ok don't worry about it ok? ok.
		//   (yeah, the easiest way to control it was to look for the no article text...)
		if (!str_contains($text, "noarticletext")) {
			return;
		}

		// janky. maybe replace this
		$title = $parser->getTitle();
		$parts = explode(":", $title);
		$source_wiki = $parts[0];

		// return to a regular title
		$source_page = str_replace(" ", "_", $parts[1]);

		// if source wiki is nil, then we can skip it
		if ( count($parts) < 2 ) {
			return;
		}

		// if we get a 404, this is a fake wiki or just... not a wiki. so we can skip it
		if ( !is_fandom_wiki($source_wiki) ) {
			return;
		}

		// grab the wikitext content through the api
		$api_endpoint_template = "https://%s.fandom.com/api.php?action=parse&format=json&page=%s";
		$api_endpoint_url = sprintf($api_endpoint_template, $source_wiki, $source_page);

		$page_json = file_get_contents($api_endpoint_url);
		$page_data = json_decode($page_json, true);

		$page_html = $page_data["parse"]["text"]["*"];

		$doc = new DOMDocument();
		$doc->loadHTML(mb_convert_encoding($page_html, 'HTML-ENTITIES', 'UTF-8'));

		// we need to clean up links and images (and those silly review blocks)
		$links = $doc->getElementsByTagName("a");
		$images = $doc->getElementsByTagName("img");
		$divs = $doc->getElementsByTagName("div");
		$svgs = $doc->getElementsByTagName("svg");

		foreach ($links as $link) {
			$new_link = str_replace("/wiki/", "/wiki/".$source_wiki.":", $link->getAttribute("href"));
			$link->setAttribute("href", $new_link);

			// clean up weird SVG gaps
			foreach ($link->childNodes as $child) {
				if ($child->tagName == "svg") {
					$link->removeChild($child);
				}
			}
		}

		foreach ($images as $image) {
			// clean image urls
			if (str_contains($image->getAttribute("class"), "lazyload")) {
				$og_source = $image->getAttribute("data-src");
				$stripped_source = explode("/revision", $og_source)[0];
				$image->setAttribute("src", $stripped_source);
			} elseif (str_contains($image->getAttribute("class"), "thumbnail")) {
				$og_source = explode(" ", $image->getAttribute("srcset"))[0];
				$stripped_source = explode("/revision", $og_source)[0];
				$image->setAttribute("src", $stripped_source);
				$image->setAttribute("srcset", "");
			} else {
				$og_source = $image->getAttribute("src");
				$stripped_source = explode("/revision", $og_source)[0];
				$image->setAttribute("src", $stripped_source);
			}
		}

		foreach ($divs as $div) {
			// get rid of advertising divs lmao
			if (str_contains($div->getAttribute("class"), "reviews")) {
				$div->parentNode->removeChild($div);
			}

			// fix galleries
			if (str_contains($div->getAttribute("class"), "wikia-gallery")) {
				$child = $div->firstChild;
				if ($child) {
					while ($child->hasChildNodes()) {
						$child->setAttribute("style", "");
						$child = $child->firstChild;
					}
				}
			}
		}

		// remove extraneous svgs
		foreach ($svgs as $svg) {
			if (str_contains($svg->parentNode->getAttribute("class"), "info-icon")) {
				$svg->parentNode->removeChild($svg);
			}
		}

		$other_injections = "";

		// inject infobox stylesheet
		$other_injections = $other_injections.'<link rel="stylesheet" href="/load.php?lang=en-ca&amp;modules=ext.PortableInfobox.styles%7Cskins.vector.styles.legacy&amp;only=styles&amp;skin=vector">';

		$text = $other_injections.$doc->saveHTML();
	}
}
